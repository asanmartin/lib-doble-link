unit LO_Doble;

interface

	const
		_BASE_NAME = ''; // puede ser vacio
		_BASE_ROOT = ''; //ruta por defecto es en donde se encuentra el exe
		_NOMBRE_CONTROL = _BASE_NAME + '.CON';
		_NOMBRE_DATOS = _BASE_NAME + '.DAT';
		_POSICION_NULA = -1;
		_FIRST_ELEMENT = 0;
		_CLAVE_NULA = '';
	//end const's
	type
		TipoPosicion = Longint;
		TipoEnlace = Longint;
		TipoPuntaje = Integer;
		TipoEstado = Byte;
		TipoClave = String[10]; // Una cadena de rango A00 - Z99
		
		TipoRegistroDatos = record
			clave :TipoClave; // Apodo del jugador
			puntaje :TipoPuntaje; // Cada jugador que se agrega al juego comienza con un puntaje de 1.000 puntos
			estado :TipoEstado; // Un digito numérico. Más adelante se explicará su uso. Inicialmente, todos en 0
			ant, sig :TipoEnlace;
		end;
		TipoArchivoDatos = file of TipoRegistroDatos;

		TipoRegistroControl = record
			primero :TipoEnlace;
			ultimo :TipoEnlace;
			borrado :TipoEnlace;
		end;
		TipoArchivoControl = file of TipoRegistroControl;

		// Creación de un Metodo
		TDobleLink = record
			c : TipoArchivoControl;
			d : TipoArchivoDatos;
		end;
	//end type's
	Procedure LO_DL_CrearDoble(var Doble:TDobleLink; nombre,ruta:String);
	Procedure LO_DL_AbrirDoble(var Doble:TDobleLink);
	Procedure LO_DL_CerrarDoble(var Doble:TDobleLink);
	Procedure LO_DL_DestruirDoble(var Doble:TDobleLink; nombre:String);
	Function LO_DL_Buscar(var Doble:TDobleLink; clave:TipoClave; var pos:TipoPosicion):Boolean;
	Procedure LO_DL_Eliminar(var Doble:TDobleLink; pos:TipoPosicion);
	Procedure LO_DL_Insertar(var Doble:TDobleLink; pos:TipoPosicion; Reg:TipoRegistroDatos);
	Procedure LO_DL_Capturar(var Doble:TDobleLink; pos:TipoPosicion; var Reg:TipoRegistroDatos);
	Procedure LO_DL_Modificar(var Doble:TDobleLink; pos:TipoPosicion; Reg:TipoRegistroDatos);
	Function LO_DL_Primero(var Doble:TDobleLink):TipoPosicion;
	Function LO_DL_Ultimo(var Doble:TDobleLink):TipoPosicion;
	Function LO_DL_Proximo(var Doble:TDobleLink; pos:TipoPosicion):TipoPosicion;
	Function LO_DL_Anterior(var Doble:TDobleLink; pos:TipoPosicion):TipoPosicion;
	Function LO_DL_DobleVacio(var Doble:TDobleLink):Boolean; //retorna TRUE si el metodo esta vacio
	Function LO_DL_PosNula(var Doble:TDobleLink):TipoPosicion;

implementation

uses SysUtils;
	Procedure LO_DL_CrearDoble(var Doble:TDobleLink; nombre,ruta:String);
		var
			bExisteCON, bExisteDAT :Boolean;
            RC :TipoRegistroControl;
		begin
			Assign ( Doble.c, ruta + nombre + '.CON');
			Assign ( Doble.d, ruta + nombre + '.DAT');

			bExisteCON := FileExists ( ruta + nombre + '.CON');
			bExisteDAT := FileExists ( ruta + nombre + '.NTX');

			if not(bExisteDAT) or not(bExisteCON) then begin
				RC.primero := _POSICION_NULA;
				RC.ultimo := _POSICION_NULA;
				RC.borrado := _POSICION_NULA;

				ReWrite(Doble.c);
				Write(Doble.c,RC);
				ReWrite(Doble.d);
				Close(Doble.c);
				Close(Doble.d);
			end;
		end;
	//end LO_DL_CrearDoble
	Procedure LO_DL_AbrirDoble(var Doble:TDobleLink);
		begin
			reset(Doble.c);
			reset(Doble.d);
		end;
	//end LO_DL_AbrirDoble
	Procedure LO_DL_CerrarDoble(var Doble:TDobleLink);
		begin
			Close(Doble.c);
			Close(Doble.d);
		end;
	//end LO_DL_CerrarDoble
	Procedure LO_DL_DestruirDoble(var Doble:TDobleLink; nombre:String);
		begin
			Assignfile(Doble.c, nombre + '.CON');
			Assignfile(Doble.d, nombre + '.DAT');
			deletefile(nombre + '.CON');
			deletefile(nombre + '.DAT');	
		end;
	//end LO_DL_DestruirDoble
	Function LO_DL_Buscar(var Doble:TDobleLink; clave:TipoClave; var pos:TipoPosicion):Boolean;
		var
			RC :TipoRegistroControl;
			Reg :TipoRegistroDatos;
			encontrado, corte :Boolean;
		begin
			Seek(Doble.c, _FIRST_ELEMENT);
			Read(Doble.c, RC);
			
			pos:= RC.primero;
			encontrado:=false;
			corte:=false;
			
			while not(corte) and not(encontrado) and (pos <> _POSICION_NULA) do begin
				Seek(Doble.d, pos);
				Read(Doble.d, Reg);

				if Reg.clave=clave then
					encontrado := true
				else 
					if Reg.clave>clave then
						corte := true
					else
						pos := Reg.sig;
					//end if
				//end if
			end;//while

			LO_DL_Buscar := encontrado;
		end;
	//end LO_DL_Buscar
	Procedure LO_DL_Eliminar(var Doble:TDobleLink; pos:TipoPosicion);
		var
			Reg, RegAnt, RegSig :TipoRegistroDatos;
			RC :TipoRegistroControl;
			posAnt, posSig :TipoPosicion;
		begin
			Seek(Doble.c, _FIRST_ELEMENT);
			Read(Doble.c, RC);
			Seek(Doble.d, pos);
			Read(Doble.d, Reg);

			if (RC.primero = pos) and (RC.ultimo = pos) then begin
				// elimino el unico elemento
				RC.primero := _POSICION_NULA;
				RC.ultimo := _POSICION_NULA;
			end else 
				if (RC.primero = pos) then begin
					// elimino al principio
					posSig := Reg.sig;
					
					Seek(Doble.d, posSig);
					Read(Doble.d, RegSig);

					RegSig.ant := _POSICION_NULA;

					RC.primero := posSig;
					Seek(Doble.d, posSig);
					Write(Doble.d, RegSig);
				end else
					if (RC.ultimo = pos) then begin
						// elimino al final
						posAnt := Reg.ant;

						Seek(Doble.d, posAnt);
						Read(Doble.d, RegAnt);

						RegAnt.sig := _POSICION_NULA;

						RC.ultimo := posAnt;
						Seek(Doble.d, posAnt);
						Write(Doble.d, RegAnt);
					end else
						// elimino una celula en el medio
						posAnt := Reg.ant;
						posSig  := Reg.sig;

						Seek(Doble.d, posAnt);
						Read(Doble.d, RegAnt);
						Seek(Doble.d, posSig);
						Read(Doble.d, RegSig);

						RegAnt.sig := posSig;
						RegSig.ant := posAnt;

						Seek(Doble.d, posAnt);
						Write(Doble.d, RegAnt);
						Seek(Doble.d, posSig);
						Write(Doble.d, RegSig);
			;// end ciclos if
			//mover el registro a borrados
			Reg.ant := _POSICION_NULA;
			Reg.sig := RC.borrado;

			RC.borrado := pos;

			Seek(Doble.d, pos);
			Write(Doble.d, Reg);
			//grabo cambios en cabecera
			Seek(Doble.c, _FIRST_ELEMENT);
			Write(Doble.c, RC);
		end;	
	//end LO_DL_Eliminar
	Procedure LO_DL_Insertar(var Doble:TDobleLink; pos:TipoPosicion; Reg:TipoRegistroDatos);
		var
			RC :TipoRegistroControl; 
			posNueva,pos1 :TipoPosicion; 
			RAux,Reg1,Reg2 :TipoRegistroDatos;
		begin
			Seek(Doble.c, _FIRST_ELEMENT);
			Read(Doble.c, RC);

			//para reciclar de datos
			if (RC.borrado =_POSICION_NULA) then begin
				posNueva := FileSize(Doble.d);
			end else begin
				posNueva := RC.borrado;
				Seek(Doble.d, posNueva);
				Read(Doble.d, RAux);
				RC.borrado := RAux.Sig;
			end;

			if (RC.primero = _POSICION_NULA) then begin
				Reg.sig := _POSICION_NULA;
				Reg.ant := _POSICION_NULA;
				RC.primero := posNueva;
				RC.ultimo := posNueva;
			end else 
				if (RC.primero = pos) then begin
					Reg.ant := _POSICION_NULA;
					Reg.sig := RC.primero;
					Seek(Doble.d, pos);
					Read(Doble.d, RAux);
					RAux.ant := posNueva;
					RC.primero := posNueva;
					Seek(Doble.d, pos);
					Write(Doble.d, RAux);
				end else
					if (pos = _POSICION_NULA) then begin
						Reg.sig := _POSICION_NULA;
						Reg.ant := RC.ultimo;
						Seek(Doble.d, RC.ultimo);
						Read(Doble.d, RAux);
						RAux.sig := posNueva;
						RC.ultimo := posNueva;
						Seek(Doble.d, Reg.ant);
						Write(Doble.d, RAux);
					end else begin
						Seek(Doble.d, pos);
						Read(Doble.d, Reg2);
						pos1 := Reg2.ant;
						Seek(Doble.d, pos1);
						Read(Doble.d, Reg1);
						Reg1.sig := posNueva;
						Reg2.ant := posNueva;
						Reg.sig := pos;
						Reg.ant := pos1;
						Seek(Doble.d, pos1);
						Write(Doble.d, Reg1);
						Seek(Doble.d, pos);
						Write(Doble.d, Reg2);
					end
			;//end ciclo if
			//grabo cabecera
			Seek(Doble.c, _FIRST_ELEMENT);
			Write(Doble.c, RC);
			
			//grabo insercion
			Seek(Doble.d, posNueva);
			Write(Doble.d, Reg);
		end;
	//end LO_DL_Insertar
	Procedure LO_DL_Capturar(var Doble:TDobleLink; pos:TipoPosicion; var Reg:TipoRegistroDatos);
		begin
			Seek(Doble.d, pos);
			Read(Doble.d, Reg);
		end;
	//end LO_DL_Capturar
	Procedure LO_DL_Modificar(var Doble:TDobleLink; pos:TipoPosicion; Reg:TipoRegistroDatos);
		var RAux :TipoRegistroDatos;
		begin
			Seek(Doble.d, pos);
			Read(Doble.d, RAux);
			Reg.ant := RAux.ant;
			Reg.sig := RAux.sig;
			Seek(Doble.d, pos);
			Write(Doble.d, Reg);
		end;
	//end LO_DL_Modificar
	Function LO_DL_Primero(var Doble:TDobleLink):TipoPosicion;
		var RC :TipoRegistroControl;
		begin
			Seek(Doble.c, _FIRST_ELEMENT);
			Read(Doble.c, RC);
			LO_DL_Primero := RC.primero;
		end;
	//end LO_DL_Primero
	Function LO_DL_Ultimo(var Doble:TDobleLink):TipoPosicion;
		var RC :TipoRegistroControl;
		begin
			Seek(Doble.c, _FIRST_ELEMENT);
			Read(Doble.c, RC);
			LO_DL_Ultimo := RC.ultimo;
		end;
	//end LO_DL_Ultimo
	Function LO_DL_Proximo(var Doble:TDobleLink; pos:TipoPosicion):TipoPosicion;
		var Reg :TipoRegistroDatos;
		begin
			Seek(Doble.d, pos);
			Read(Doble.d, Reg);
			LO_DL_Proximo := Reg.sig;
		end;
	//end LO_DL_Proximo
	Function LO_DL_Anterior(var Doble:TDobleLink; pos:TipoPosicion):TipoPosicion;
		var Reg :TipoRegistroDatos;
		begin
			Seek(Doble.d, pos);
			Read(Doble.d, Reg);
			LO_DL_Anterior := Reg.ant;
		end;	
	//end LO_DL_Anterior
	Function LO_DL_DobleVacio(var Doble:TDobleLink):Boolean;
		var RC :TipoRegistroControl;
		begin
			Seek(Doble.c, _FIRST_ELEMENT);
			Read(Doble.c, RC);
			LO_DL_DobleVacio := (RC.primero = _POSICION_NULA);
		end;
	//end LO_DL_DobleVacio
	Function LO_DL_PosNula(var Doble:TDobleLink):TipoPosicion;
		begin
			LO_DL_PosNula := _POSICION_NULA;
		end;
	//end LO_DL_PosNula
end.
